using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Planet : MonoBehaviour
{
    public Rigidbody rb;
    public float orbitSpeedVelocity = 5f;
    public float orbitRadius;
    public float currentAngle = 0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        orbitRadius = transform.localPosition.magnitude;
    }
}
