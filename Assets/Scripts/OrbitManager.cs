using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitManager : MonoBehaviour
{

    Planet[] allPlanets;

    // Start is called before the first frame update
    void Start()
    {
        allPlanets = GetComponentsInChildren<Planet>();
    }

    void FixedUpdate()
    {
        foreach (Planet p in allPlanets)
        {
            float angleDelta = Mathf.Rad2Deg * Mathf.Atan(p.orbitSpeedVelocity / p.orbitRadius) * Time.deltaTime;
            p.currentAngle += angleDelta;
            Vector3 posDelta = Quaternion.Euler(0f, p.currentAngle, 0f) * Vector3.forward * p.orbitRadius - p.rb.position;
            p.rb.position += posDelta;
            Rigidbody[] childRBs = p.transform.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody child in childRBs)
            {
                if (child == p.rb) continue;
                child.position += posDelta;
            }
        }
    }
}
