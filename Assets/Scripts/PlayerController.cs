using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    Transform gravitySource;
    Rigidbody rb;

    Transform camTransform;


    void Start()
    {
        gravitySource = transform.parent;
        rb = GetComponent<Rigidbody>();
        camTransform = transform.GetChild(0);

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(transform.up * 2f, ForceMode.VelocityChange);
            Debug.Log("Jump");
        }

        float rot = camTransform.localEulerAngles.y;
        rot += Input.GetAxis("Mouse X");
        if (rot > 360f) rot -= 360f;
        if (rot < -360f) rot += 360f;

        camTransform.localEulerAngles = new Vector3(0f, rot, 0f);
    }

    void FixedUpdate()
    {
        Vector3 downVector = (gravitySource.position - rb.position).normalized;
        rb.AddForce(downVector * 40f * Time.deltaTime);

        Vector3 translateVector = camTransform.right * ((Input.GetKey(KeyCode.A) ? -1f : 0f) + (Input.GetKey(KeyCode.D) ? 1f : 0f)) + camTransform.forward * ((Input.GetKey(KeyCode.S) ? -1f : 0f) + (Input.GetKey(KeyCode.W) ? 1f : 0f));

        rb.AddForce(translateVector * 5f);

        transform.up = -downVector;
    }

}
